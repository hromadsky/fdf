/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 18:11:18 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/23 18:11:49 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		diag_x(t_fdf *f, t_pxl a, t_pxl b)
{
	int e;

	e = (f->dy << 1) - f->dx;
	while (a.x != b.x)
	{
		if (e < 0)
		{
			f->sx ? a.x++ : a.x--;
			e += (f->dy << 1);
			fdf_mlx_img(f, a);
		}
		else
		{
			f->sy ? a.y++ : a.y--;
			e -= f->dx << 1;
		}
	}
}

static void		diag_y(t_fdf *f, t_pxl a, t_pxl b)
{
	int e;

	e = (f->dx << 1) - f->dy;
	while (a.y != b.y)
	{
		if (e < 0)
		{
			f->sy ? a.y++ : a.y--;
			e += (f->dx << 1);
			fdf_mlx_img(f, a);
		}
		else
		{
			f->sx ? a.x++ : a.x--;
			e -= (f->dy << 1);
		}
	}
}

static void		get_line_type(t_fdf *f, t_pxl a, t_pxl b)
{
	f->sx = (b.x - a.x) > 0;
	f->sy = (b.y - a.y) > 0;
	if (f->sx)
		f->dx = (b.x - a.x);
	else
		f->dx = (a.x - b.x);
	if (f->sy)
		f->dy = (b.y - a.y);
	else
		f->dy = (a.y - b.y);
	fdf_mlx_img(f, a);
	if (f->dx > f->dy)
		diag_x(f, a, b);
	else
		diag_y(f, a, b);
}

void			img_gen(t_fdf *fdf)
{
	int x;
	int y;

	recalc(fdf, 0, 0);
	fdf->mlx.img = mlx_new_image(fdf->mlx.mlx, fdf->win_x, fdf->win_y);
	y = 0;
	while (y < fdf->map_y)
	{
		x = 0;
		while (x < fdf->map_x)
		{
			if (y != fdf->map_y - 1)
				get_line_type(fdf, fdf->map[y][x], fdf->map[y + 1][x]);
			if (x != fdf->map_x - 1)
				get_line_type(fdf, fdf->map[y][x], fdf->map[y][x + 1]);
			x++;
		}
		y++;
	}
	mlx_put_image_to_window(fdf->mlx.mlx, fdf->mlx.win, fdf->mlx.img, 0, 0);
	mlx_destroy_image(fdf->mlx.mlx, fdf->mlx.img);
	(fdf->show_interface) ? fdf_screeninfo(fdf) : (0);
	(fdf->show_how_to_use) ? how_to_use(fdf) :
	mlx_string_put(fdf->mlx.mlx, fdf->mlx.win, 5, fdf->win_y - 25,
	0xFDF9FF, "(H)elp");
}
