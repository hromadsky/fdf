/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interface.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 18:04:20 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/23 18:04:22 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		zosphe_data(t_fdf *f)
{
	char	*tmp;
	char	*vtmp;
	char	*zoom;

	vtmp = ft_itoa(f->height);
	tmp = ft_strjoin("Height multiplier: ", vtmp);
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 0, 0xFDF9FF, tmp);
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	vtmp = ft_itoa(f->speed);
	tmp = ft_strjoin("Speed:", vtmp);
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 30, 0xFDF9FF, tmp);
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	zoom = ft_itoa(f->zoom);
	tmp = ft_strjoin("Zoom:", zoom);
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 15, 0xFDF9FF, tmp);
	tmp ? ft_memdel((void *)&tmp) : (0);
	zoom ? ft_memdel((void *)&zoom) : (0);
}

static void		xyz_data(t_fdf *f, int off_x)
{
	char	*tmp;
	char	*vtmp;
	char	*rt;

	tmp = NULL;
	vtmp = ft_itoa(f->pos_x);
	tmp = ft_strjoin("X:", vtmp);
	rt = ft_strjoin(tmp, " Y:");
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	vtmp = ft_itoa(f->pos_y);
	tmp = ft_strjoin(rt, vtmp);
	rt ? ft_memdel((void *)&rt) : (0);
	rt = ft_strjoin(tmp, " Z:");
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	vtmp = ft_itoa(f->pos_z);
	tmp = ft_strjoin(rt, vtmp);
	off_x = (f->win_x - (ft_strlen(tmp) * 10)) / 2;
	mlx_string_put(f->mlx.mlx, f->mlx.win, off_x, 0, 0xCC0033, tmp);
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	rt ? ft_memdel((void *)&rt) : (0);
}

static void		mapsize_data(t_fdf *f)
{
	char	*tmp;
	char	*vtmp;
	char	*rt;
	int		offset;

	vtmp = ft_itoa(f->map_x);
	rt = ft_strjoin("cols:", vtmp);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
	tmp = ft_strjoin(rt, " rows:");
	rt ? ft_memdel((void *)&rt) : (0);
	vtmp = ft_itoa(f->map_y);
	rt = ft_strjoin(tmp, vtmp);
	offset = (f->win_x - (ft_strlen(rt) * 10)) / 2;
	mlx_string_put(f->mlx.mlx, f->mlx.win, offset, f->win_y - 40, 0x00ceFF, rt);
	rt ? ft_memdel((void *)&rt) : (0);
	tmp ? ft_memdel((void *)&tmp) : (0);
	vtmp ? ft_memdel((void *)&vtmp) : (0);
}

void			fdf_screeninfo(t_fdf *f)
{
	char	*tmp;
	int		offset;

	tmp = NULL;
	tmp = ft_strjoin("MAP:", f->name);
	offset = (f->win_x - (ft_strlen(tmp) * 10)) / 2;
	mlx_string_put(f->mlx.mlx, f->mlx.win, offset, f->win_y - 20,
	0x00FF00, tmp);
	tmp ? ft_memdel((void *)&tmp) : (0);
	mapsize_data(f);
	xyz_data(f, 0);
	zosphe_data(f);
}
