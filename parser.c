/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 17:13:07 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/24 15:26:52 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

static char	*get_color(char *str)
{
	int		i;
	int		end;
	char	*tmp;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == ',')
			break ;
		i++;
	}
	if (str[i] == ',')
	{
		end = i;
		while (str[end])
			end++;
		tmp = ft_strsub(str, i, end);
		return (tmp);
	}
	return (NULL);
}

static void	put_height(t_fdf *f, int x, int y, char **elems)
{
	char *tmp;

	f->map[y][x].h1 = ft_atoi(elems[x]);
	f->map[y][x].h1 > f->map_max ? f->map_max = f->map[y][x].h1 : 0;
	f->map[y][x].h1 < f->map_min ? f->map_min = f->map[y][x].h1 : 0;
	tmp = get_color(elems[x]);
	tmp ? (f->map[y][x].color = ft_hex_to_int(tmp))
		: (f->map[y][x].color = 0xFFAA00);
	tmp ? ft_memdel((void *)&tmp) : (0);
}

static void	save_heights(t_fdf *f, int x, int y)
{
	char	*line;
	char	**elems;
	int		fd;

	if ((fd = open(f->name, O_RDONLY)) < 0)
		fdf_error(ERR_OPEN);
	y = 0;
	while (get_next_line(fd, &line))
	{
		elems = ft_strsplit(line, ' ');
		line ? ft_memdel((void *)&line) : (0);
		x = 0;
		while (x < f->map_x && elems[x])
		{
			put_height(f, x, y, elems);
			x++;
		}
		elems ? array_del(elems) : (0);
		y++;
	}
	close(fd);
}

static void	fdf_malloc(t_fdf *f, int i)
{
	if (!(f->map = (t_pxl **)malloc(sizeof(t_pxl *) * f->map_y)))
		fdf_error(ERR_MALLOC);
	while (i < f->map_y)
		if (!(f->map[i++] = (t_pxl *)malloc(sizeof(t_pxl) * f->map_x)))
			fdf_error(ERR_MALLOC);
	save_heights(f, 0, 0);
}

void		parser(t_fdf *fdf)
{
	char	*line;
	char	**elems;
	char	*tmp;
	int		fd;

	if ((fd = open(fdf->name, O_RDONLY)) < 0)
		fdf_error(ERR_OPEN);
	if ((read(fd, NULL, 0)) < 0)
		fdf_error(ERR_OPEN);
	while (get_next_line(fd, &line))
	{
		tmp = ft_strtrim(line);
		elems = ft_strsplit(tmp, ' ');
		tmp ? ft_memdel((void *)&tmp) : (0);
		line ? ft_memdel((void *)&line) : (0);
		(fdf->map_x == 0) ? fdf->map_x = count_elem(elems) : (0);
		if (fdf->map_x != ft_sstrlen(elems))
			fdf_error(ERR_NOT_EQU);
		elems ? array_del(elems) : (0);
		fdf->map_y++;
	}
	(fdf->map_y > fdf->map_x) ? fdf->map_m = fdf->map_y
	: (fdf->map_m = fdf->map_x);
	close(fd);
	fdf_malloc(fdf, 0);
}
