/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 14:35:00 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/24 14:35:22 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "keys.h"

static void	position_to_360(t_fdf *f)
{
	f->pos_x >= 360 ? f->pos_x -= 360 : (0);
	f->pos_y >= 360 ? f->pos_y -= 360 : (0);
	f->pos_z >= 360 ? f->pos_z -= 360 : (0);
	f->pos_x <= -360 ? f->pos_x += 360 : (0);
	f->pos_y <= -360 ? f->pos_y += 360 : (0);
	f->pos_z <= -360 ? f->pos_z += 360 : (0);
}

int			keyhook(int key, t_fdf *f)
{
	position_to_360(f);
	(key == ESC) ? exit(0) : (0);
	(key == KEY_I) ? f->show_interface = !f->show_interface : (0);
	(key == NUM5) ? defaults(f) : (0);
	(key == KEY_H) ? f->show_how_to_use = !f->show_how_to_use : (0);
	(key == NUM9) ? f->height++ : (0);
	(key == NUM7 && f->height > 1) ? f->height-- : (0);
	(key == NUM_P && f->speed < 10) ? f->speed++ : (0);
	(key == NUM_N && f->speed) ? f->speed-- : (0);
	(key == NUM8) ? f->pos_x += f->speed : (0);
	(key == NUM2) ? f->pos_x -= f->speed : (0);
	(key == NUM6) ? f->pos_y += f->speed : (0);
	(key == NUM4) ? f->pos_y -= f->speed : (0);
	(key == NUM1) ? f->pos_z += f->speed : (0);
	(key == NUM3) ? f->pos_z -= f->speed : (0);
	(key == NUM0) ? f->zoom += 1 : (0);
	(key == NUM_DOT && f->zoom > 1) ? f->zoom -= 1 : (0);
	((f->height * f->map_max) > (f->win_y / 2)) ? f->height-- : (0);
	(((f->zoom) * f->map_m) > (f->win_y)) ? f->zoom-- : (0);
	img_gen(f);
	return (0);
}
