/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_del.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 18:29:26 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/23 18:35:50 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	array_del(char **s)
{
	int i;

	i = 0;
	while (s[i])
		ft_memdel((void *)&s[i++]);
	ft_memdel((void *)&s);
}
