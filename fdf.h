/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 17:13:35 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/24 14:41:25 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "fcntl.h"
# include "mlx.h"
# include "Libft/libft.h"
# include <stddef.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>

# define ERR_ARG		"Wrong number of arguments. Usage: ./test maps.fdf"
# define ERR_OPEN	"File error: Failed to open the file."
# define ERR_MALLOC		"Malloc Error."
# define ERR_NOT_EQU	"Map error: Lines are not equal in length."

typedef struct	s_line
{
	float		a;
	float		b;
	float		c;
}				t_line;

typedef struct	s_pxl
{
	int			x;
	int			y;
	int			h;
	int			x1;
	int			y1;
	int			h1;
	int			color;
}				t_pxl;

typedef struct	s_mlx
{
	void		*mlx;
	void		*win;
	void		*img;
}				t_mlx;

typedef struct	s_fdf
{
	char		*name;
	t_mlx		mlx;
	int			win_x;
	int			win_y;
	t_pxl		**map;
	int			map_x;
	int			map_y;
	int			map_m;
	int			map_max;
	int			map_min;
	int			height;
	int			zoom;
	int			speed;
	float		pos_x;
	float		pos_y;
	float		pos_z;
	int			dx;
	int			dy;
	int			sx;
	int			sy;
	int			show_interface;
	int			show_how_to_use;
}				t_fdf;

void			parser(t_fdf *f);
void			recalc(t_fdf *f, int x, int y);
void			img_gen(t_fdf *fdf);
void			fdf_mlx_img(t_fdf *f, t_pxl a);
void			fdf_error(char *reason);
void			fdf_screeninfo(t_fdf *f);
void			how_to_use(t_fdf *f);
int				keyhook(int key, t_fdf *f);
void			defaults(t_fdf *f);

#endif
