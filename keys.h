/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 19:36:33 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/23 19:37:22 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

# define NUM0     	82
# define NUM_DOT  	65
# define NUM1		83
# define NUM2		84
# define NUM3		85
# define NUM4		86
# define NUM5		87
# define NUM6		88
# define NUM7		89
# define NUM8		91
# define NUM9		92
# define NUM_P		69
# define NUM_N		78
# define KEY_I		34
# define KEY_H		4
# define ESC		53

#endif
