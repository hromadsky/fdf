/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_calculate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 18:10:05 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/23 18:10:32 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <math.h>

static void		shifting(t_fdf *f, t_pxl *px)
{
	float	angle;
	float	x[2];

	angle = (3.14159265 * f->pos_x) / 180;
	x[0] = px->h * cos(angle) + px->y1 * sin(angle);
	x[1] = px->y1 * cos(angle) - px->h * sin(angle);
	px->h = round(x[0]);
	px->y1 = round(x[1]);
	angle = (3.14159265 * f->pos_y) / 180;
	x[0] = px->x1 * cos(angle) + px->h * sin(angle);
	x[1] = px->h * cos(angle) - px->x1 * sin(angle);
	px->x1 = round(x[0]);
	px->h = round(x[1]);
	angle = (3.14159265 * f->pos_z) / 180;
	x[0] = px->x1 * cos(angle) + px->y1 * sin(angle);
	x[1] = px->y1 * cos(angle) - px->x1 * sin(angle);
	px->x1 = round(x[0]);
	px->y1 = round(x[1]);
}

void			recalc(t_fdf *f, int x, int y)
{
	t_line	tmp;

	y = 0;
	while (y < f->map_y)
	{
		x = 0;
		while (x < f->map_x)
		{
			f->map[y][x].x1 = f->zoom * (x - f->map_x / 2);
			f->map[y][x].y1 = f->zoom * (y - f->map_y / 2);
			f->map[y][x].h = f->map[y][x].h1 * f->height;
			shifting(f, &f->map[y][x]);
			tmp.a = f->map[y][x].x1;
			tmp.b = f->map[y][x].y1;
			tmp.c = f->map[y][x].h - 1000;
			f->map[y][x].x = (tmp.a / tmp.c) * (250 - 1000);
			f->map[y][x].x += f->win_x / 2;
			f->map[y][x].y = (tmp.b / tmp.c) * (250 - 1000);
			f->map[y][x].y += f->win_y / 2;
			x++;
		}
		y++;
	}
}

void			fdf_mlx_img(t_fdf *f, t_pxl a)
{
	char	*px;
	int		bpp;
	int		endian;
	int		len;

	if (a.x >= f->win_x || a.y >= f->win_y || a.x < 0 || a.y < 0)
		return ;
	px = mlx_get_data_addr(f->mlx.img, &bpp, &len, &endian);
	if (endian == 0)
		((int *)px)[a.y * len / 4 + a.x] = a.color;
}
