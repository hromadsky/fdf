/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 17:08:01 by dhromads          #+#    #+#             */
/*   Updated: 2018/07/24 14:36:22 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		how_to_use(t_fdf *f)
{
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 55, 0xFDF9FF,
	"NUM7/NUM9 - Height multiplier -/+");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 70, 0xFDF9FF,
	"NUM1/NUM3 - Rotate -/+");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 85, 0xFDF9FF,
	"NUM4/NUM6 - X -/+");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 100, 0xFDF9FF,
	"NUM2/NUM8 - Y -/+");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 115, 0xFDF9FF,
	"NUM0/NUM., - Zoom +/-");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 130, 0xFDF9FF,
	"NUM+/NUM- - Speed +/-");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 145, 0xFDF9FF,
	"NUM5 - Reset");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 160, 0xFDF9FF,
	"I - Show/hide info");
	mlx_string_put(f->mlx.mlx, f->mlx.win, 5, 175, 0xFDF9FF,
	"H - Show/hide How to use");
}

void		fdf_error(char *reason)
{
	ft_putendl_fd(reason, 2);
	exit(EXIT_FAILURE);
}

void		defaults(t_fdf *f)
{
	f->win_x = ((f->map_x > f->map_y) ? f->map_x : f->map_y) * 10;
	(f->win_x < 800) ? f->win_x = 800 : (0);
	(f->win_x > 1920) ? f->win_x = 1920 : (0);
	f->win_y = (f->win_x * 9) / 16;
	f->zoom = f->win_y / ((f->map_x > f->map_y) ? f->map_x : f->map_y);
	if (!f->zoom)
		f->zoom = 2;
	f->height = 1;
	f->pos_x = 30;
	f->pos_y = 0;
	f->pos_z = 0;
	f->speed = 3;
}

int			main(int ac, char **av)
{
	t_fdf fdf;

	if (ac != 2)
		fdf_error(ERR_ARG);
	fdf.map_x = 0;
	fdf.map_y = 0;
	fdf.map_max = 0;
	fdf.map_min = 0;
	fdf.show_interface = 0;
	fdf.show_how_to_use = 0;
	fdf.name = ft_strdup(av[1]);
	parser(&fdf);
	defaults(&fdf);
	fdf.mlx.mlx = mlx_init();
	fdf.mlx.win = mlx_new_window(fdf.mlx.mlx, fdf.win_x, fdf.win_y, "FDF");
	img_gen(&fdf);
	mlx_hook(fdf.mlx.win, 2, (1L << 0), &keyhook, &fdf);
	mlx_loop(fdf.mlx.mlx);
	return (0);
}
